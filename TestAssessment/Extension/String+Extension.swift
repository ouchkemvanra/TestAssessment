//
//  String+Extension.swift
//  TestAssessment
//
//  Created by Ouch Kemvanra on 5/27/21.
//

import Foundation
extension String{
    func toDateString(dateFormat format: String = "dd MM YYYY") -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat =  format
        return dateFormatter.string(from: toDate())
    }
    func toDate(format: String = "yyyy-MM-dd'T'HH:mm:ss.SSSZ") -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat =  format
        if let date = dateFormatter.date(from: self) {
            return date
        }
        return Date()
    }
}
