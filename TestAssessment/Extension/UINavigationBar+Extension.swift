//
//  UINavigationBar+Extension.swift
//  TestAssessment
//
//  Created by Ouch Kemvanra on 5/27/21.
//

import UIKit
extension UINavigationBar{
    func barTransparent(_ color: UIColor = .init(hexFromString: "#042949")){
        self.setBackgroundImage(UIImage(), for: .default)
        self.hideBottomHairline()
        self.isTranslucent = false
        self.tintColor = .white
        self.barTintColor = color
        self.titleTextAttributes =  [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14)]
    }
    func hideBottomHairline() {
        self.hairlineImageView?.isHidden = true
    }

    func showBottomHairline() {
        self.hairlineImageView?.isHidden = false
    }
}

