//
//  UITextView+Extension.swift
//  TestAssessment
//
//  Created by Ouch Kemvanra on 5/27/21.
//

import UIKit
extension UITextView{
    func setMultitpleTextAttribute(textArray: [String], textColorArray: [UIColor], fontArray: [UIFont]){
      /// Loop Text Array
        let attrArray = NSMutableAttributedString.init(string: "")
      for i in 0..<textArray.count{
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 8
        let attrs = [NSAttributedString.Key.font : fontArray[i], NSAttributedString.Key.foregroundColor : textColorArray[i], NSAttributedString.Key.paragraphStyle : style]
        let attrText = NSMutableAttributedString.init(string: textArray[i], attributes: attrs)
        attrArray.append(attrText)
      }
      self.attributedText = attrArray
    }
}
