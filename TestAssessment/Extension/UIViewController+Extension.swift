//
//  UIViewController+Extension.swift
//  TestAssessment
//
//  Created by Ouch Kemvanra on 5/28/21.
//

import UIKit
typealias CompletionHandler = (()->())
extension UIViewController{
    func alert(_ title: String, actions: [UIAlertAction] = [UIAlertAction(title: "Ok", style: .default, handler: nil)], completion: CompletionHandler? = nil) {
        let alertController = UIAlertController(title: title, message: nil, preferredStyle: .alert)
        for action in actions {
            alertController.addAction(action)
        }
        self.present(alertController, animated: true) {
            completion?()
        }
    }
}
