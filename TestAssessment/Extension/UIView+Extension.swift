//
//  UIView+Extension.swift
//  TestAssessment
//
//  Created by Ouch Kemvanra on 5/27/21.
//

import UIKit
// MARK: Navigation Hair line
extension UIView {
    var hairlineImageView: UIImageView? {
        return hairlineImageView(in: self)
    }

    func hairlineImageView(in view: UIView) -> UIImageView? {
        if let imageView = view as? UIImageView, imageView.bounds.height <= 1.0 {
            return imageView
        }

        for subview in view.subviews {
            if let imageView = self.hairlineImageView(in: subview) { return imageView }
        }

        return nil
    }
    
    // MARK: - Add Shadow
    func addShadow(offset: CGSize, color: UIColor, radius: CGFloat, opacity: Float) {
        layer.masksToBounds = false
        layer.shadowOffset = offset
        layer.shadowColor = color.cgColor
        layer.shadowRadius = radius
        layer.shadowOpacity = opacity
        let backgroundCGColor = backgroundColor?.cgColor
        backgroundColor = nil
        layer.backgroundColor =  backgroundCGColor
    }
    
    //MARK: -  Make view circular
    func makeCircular() {
        let radius = self.bounds.size.height / 2.0
        self.layer.cornerRadius = radius
        self.clipsToBounds = true
    }
}
