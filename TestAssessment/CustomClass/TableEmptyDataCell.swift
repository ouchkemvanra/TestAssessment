//
//  TableEmptyDataCell.swift
//  TestAssessment
//
//  Created by Ouch Kemvanra on 5/26/21.
//

import UIKit
class EmptyData: Codable{
    var title : String = "Empty Data"
    var description: String = "You can add data by clicking on add button below"
    var buttonTitle: String = "Add"
}
class TableEmptyDataCell: BaseTableViewCell<EmptyData> {
    
    private lazy var titleTextView: UITextView = {
        let txv = UITextView.init()
        txv.font = txv.font?.withSize(14)
        txv.textColor = .white
        txv.isScrollEnabled = false
        txv.isUserInteractionEnabled = false
        txv.textAlignment = .center
        txv.backgroundColor = .clear
        txv.translatesAutoresizingMaskIntoConstraints = false
        return txv
    }()
    private lazy var descTextView: UITextView = {
        let txv = UITextView.init()
        txv.font = txv.font?.withSize(12)
        txv.textColor = .white
        txv.isScrollEnabled = false
        txv.isUserInteractionEnabled = false
        txv.textAlignment = .center
        txv.backgroundColor = .clear
        txv.translatesAutoresizingMaskIntoConstraints = false
        return txv
    }()
    private lazy var addButton: UIButton = {
        let btn = UIButton.init()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.imageView?.contentMode = .scaleAspectFit
        btn.layer.cornerRadius = 10
        btn.clipsToBounds = true
        btn.backgroundColor = .init(hexFromString: "#085191")
        btn.addTarget(self, action: #selector(didTapOnAdd), for: .touchUpInside)
        return btn
    }()
    
    override func setView() {
        selectionStyle = .none
        contentView.backgroundColor = .init(hexFromString: "#042949")
        contentView.addSubview(descTextView)
        contentView.addSubview(titleTextView)
        contentView.addSubview(addButton)
        
        NSLayoutConstraint.activate([
            descTextView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            descTextView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 8),
            descTextView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -8),
            
            titleTextView.bottomAnchor.constraint(equalTo: descTextView.topAnchor),
            titleTextView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 8),
            titleTextView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -8),
            
            addButton.topAnchor.constraint(equalTo: descTextView.bottomAnchor, constant: 8),
            addButton.widthAnchor.constraint(equalToConstant: 120),
            addButton.heightAnchor.constraint(equalToConstant: 40),
            addButton.centerXAnchor.constraint(equalTo: contentView.centerXAnchor)
        ])
    }
    override func configureData(_ data: EmptyData, index: IndexPath) {
        titleTextView.text = data.title
        descTextView.text = data.description
        addButton.setTitle(data.buttonTitle, for: .normal)
    }
}
extension TableEmptyDataCell{
    @objc private func didTapOnAdd(){
        userActionOnCell?(.init(), "Add")
    }
}
