//
//  GenericDataSource.swift
//  TestAssessment
//
//  Created by Ouch Kemvanra on 5/25/21.
//

import Foundation
import SimpleTwoWayBinding
typealias HandleAction<D:Any, TAG: Any> = ((_ item: D, _ tag: TAG)->())
class GenericDataSource<T> : NSObject {
    var didSelectOnItem: ((_ item: T, _ indexPath: IndexPath)->Void)?

    var didActionOnItem: HandleAction<T?, Any>?
    
    var data: Observable<[T]> = Observable()
}
