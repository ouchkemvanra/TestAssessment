//
//  CircularImageView.swift
//  TestAssessment
//
//  Created by Ouch Kemvanra on 5/28/21.
//

import UIKit
@IBDesignable public class CircularImageView: UIImageView {

    override public func layoutSubviews() {
        super.layoutSubviews()
        //hard-coded this since it's always round
        layer.cornerRadius = 0.5 * bounds.size.width
    }
}
