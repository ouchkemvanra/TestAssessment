//
//  Tabbar.swift
//  TestAssessment
//
//  Created by Ouch Kemvanra on 5/25/21.
//
//085191
//8D8D8D

import UIKit
import STTabbar
class TabbarVC: UITabBarController, UITabBarControllerDelegate {

    override func viewDidLoad() {
        let newBar = STTabbar(frame: tabBar.frame)
        newBar.buttonImage = #imageLiteral(resourceName: "plus")
        newBar.tabbarColor = .white
        newBar.unselectedItemColor = .gray
        newBar.centerButtonActionHandler = {[weak self] in
            
            if let nav = self?.viewControllers?.first as? UINavigationController, let vc = nav.viewControllers.first(where: {$0.isKind(of: HomeView.self)}) as? HomeView{
                vc.addEmployee()
            }
        }
        setValue(newBar, forKey: "tabBar")
        let homeView = setView(vc: HomeView(), image: #imageLiteral(resourceName: "home"), selectedImage: #imageLiteral(resourceName: "home_blue"), title: "Home")
        let moreView = setView(vc: MoreView(), image: #imageLiteral(resourceName: "more"), selectedImage: #imageLiteral(resourceName: "more_blue"), title: "More")
        viewControllers = [homeView, moreView]
    }
    
    // Set TabbarItem for each viewcontroller
    private func setView(vc: UIViewController, image: UIImage, selectedImage: UIImage, title: String) -> UINavigationController{
        let nav = UINavigationController.init(rootViewController: vc)
        nav.tabBarItem.title = title
        nav.tabBarItem.image = image
        nav.tabBarItem.selectedImage = selectedImage
        return nav
    }
}
