//
//  State.swift
//  TestAssessment
//
//  Created by Ouch Kemvanra on 5/25/21.
//

import Foundation
enum State{
  case fetching
  case error(_ error: Error)
  case success
}
