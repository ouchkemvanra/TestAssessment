//
//  BottomPopupViewController.swift
//  TestAssessment
//
//  Created by Ouch Kemvanra on 5/27/21.
//

import UIKit
import Foundation
import BottomPopup
protocol PopupActionDelegate: class {
    func action(_ type: PopUpActionType, data: Any)
}
class BottomPopupView: BottomPopupViewController {
    var isUploadStyle = false
    var data: [PopupData] = []
    var computeData: Any?
    lazy var tableView: UITableView = {
        let tableView = UITableView.init(frame: .zero)
        tableView.register(ProfilePictureActionListCell.self, forCellReuseIdentifier: "cellId")
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorStyle = .none
        tableView.isScrollEnabled = false
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()
    weak var delegate: PopupActionDelegate?
    override func viewDidLoad() {
    super.viewDidLoad()
        view.backgroundColor = .white
        view.addSubview(tableView)
        NSLayoutConstraint.activate([
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -34),
            tableView.topAnchor.constraint(equalTo: view.topAnchor)
        ])
    }
    override var popupHeight: CGFloat { return CGFloat(data.count * 60) + 34 }
    override var popupTopCornerRadius: CGFloat { return CGFloat(10) }
    override var popupDimmingViewAlpha: CGFloat { return 0.4 }
    override var popupPresentDuration: Double { return 0.3 }
    override var popupDismissDuration: Double { return 0.3 }
}
extension BottomPopupView: UITableViewDataSource{
      func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  data.count
      }
      func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellId") as! ProfilePictureActionListCell
        let row = indexPath.row
        cell.configure(data: data[row], indexPath: indexPath)
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
        return cell
      }
}
extension BottomPopupView: UITableViewDelegate{
      func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 50
      }
      func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            self.dismiss(animated: true){
                let row = indexPath.row
                let type = self.data[row].type
                self.delegate?.action(type, data: self.computeData)
            }
      }
}


class PopupData: Decodable {
    var type: PopUpActionType = .none
    var image: UIImage?
    var title: String?
    init(_ type: PopUpActionType, image: UIImage?, title: String) {
        self.type = type
        self.image = image
        self.title = title
    }
    enum CodingKeys: String, CodingKey {
      case title = "title"
    }
}

enum PopUpActionType {
    case edit
    case delete
    case photoGallery
    case camera
    case none
}

class ProfilePictureActionListCell: BaseTableViewCell<PopupData> {
    private lazy var titleLabel: UILabel = {
        let lb = UILabel.init(frame: .zero)
        lb.font = .systemFont(ofSize: 14)
        lb.translatesAutoresizingMaskIntoConstraints = false
        lb.numberOfLines = 1
        lb.text = "upload via camera"
        return lb
    }()
    private lazy var iconImageView: UIImageView = {
        let imv = UIImageView.init(frame: .zero)
        imv.contentMode = .scaleAspectFit
        imv.translatesAutoresizingMaskIntoConstraints = false
//        imv.image = #imageLiteral(resourceName: "ic_camera")
        return imv
    }()
    private lazy var imageBackgroundView: UIView = {
        let view = UIView.init(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .lightGray
        view.layer.cornerRadius = 15
        view.clipsToBounds = true
        return view
    }()
    override func setView() {
        contentView.backgroundColor = .white
        addSubview(titleLabel)
        addSubview(imageBackgroundView)
        addSubview(iconImageView)

        NSLayoutConstraint.activate([
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 60),
            titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8),
            titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: 8),
            titleLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8),
            
            imageBackgroundView.widthAnchor.constraint(equalToConstant: 30),
            imageBackgroundView.heightAnchor.constraint(equalToConstant: 30),
            imageBackgroundView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            imageBackgroundView.centerYAnchor.constraint(equalTo: centerYAnchor),
            
            iconImageView.centerYAnchor.constraint(equalTo: imageBackgroundView.centerYAnchor),
            iconImageView.centerXAnchor.constraint(equalTo: imageBackgroundView.centerXAnchor),
            iconImageView.widthAnchor.constraint(equalTo: imageBackgroundView.widthAnchor, constant: -12),
            iconImageView.heightAnchor.constraint(equalTo: imageBackgroundView.heightAnchor, constant: -12)
        ])
    }
    
    func configure(data: PopupData, indexPath: IndexPath) {
        self.iconImageView.image = data.image
        self.titleLabel.text = data.title
    }
}
