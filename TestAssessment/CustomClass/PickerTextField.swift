//
//  PickerTextField.swift
//  TestAssessment
//
//  Created by Ouch Kemvanra on 5/27/21.
//

import UIKit
import SimpleTwoWayBinding
import SkyFloatingLabelTextField
protocol PickerTextFieldDelegate: class {
  func textFieldDidChange(_ textField: UITextField)
}
class PickerTextField: UIView {
  /// Outlet
    lazy var textField: SkyFloatingLabelTextField = {
        let txf = SkyFloatingLabelTextField.init()
        txf.delegate = self
        txf.titleFont = .systemFont(ofSize: 10)
        txf.font = .systemFont(ofSize: 10)
        txf.translatesAutoresizingMaskIntoConstraints = false
        return txf
    }()
    lazy var icon_imv: UIImageView = {
        let imv = UIImageView.init(frame: .init(x: 0, y: 0, width: 30, height: 30))
        imv.translatesAutoresizingMaskIntoConstraints = false
        imv.contentMode = .scaleAspectFit
        return imv
    }()

    /// Picker View
    var pickerView: UIPickerView?
    var datePickerView: UIDatePicker?
    /// Data Source
    weak var delegate: PickerTextFieldDelegate?
    var dataSource = PickerTextFieldDataSource()
    /// Store Properties
    var isDatePickerStyle = false{
    didSet{
      if isDatePickerStyle{
        setupDatePicker()
      }
    }
    }
    var selectedIndex = 0
    /// Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }

    init(_ title: String, image: UIImage){
        super.init(frame: .zero)
        commonInit()
        self.textField.placeholder = title
        self.icon_imv.image = image
    }
    /// Private Method
    /**
    Common Initializer for View
    */
    private func commonInit(){
        textField.font = .systemFont(ofSize: 11)

        addSubview(textField)
        addSubview(icon_imv)
        NSLayoutConstraint.activate([
            textField.leadingAnchor.constraint(equalTo: leadingAnchor),
            textField.trailingAnchor.constraint(equalTo: trailingAnchor),
            textField.topAnchor.constraint(equalTo: topAnchor),
            textField.bottomAnchor.constraint(equalTo: bottomAnchor),
            
            icon_imv.widthAnchor.constraint(equalToConstant: 20),
            icon_imv.heightAnchor.constraint(equalToConstant: 10),
            icon_imv.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0),
            icon_imv.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 4)
        ])
        setupPickerView()
    }
    /**
    Setup Picker View
    */
    private func setupPickerView(){
        pickerView = UIPickerView.init(frame: .zero)
        pickerView?.dataSource = dataSource
        pickerView?.delegate = self
        pickerView?.observe(for: dataSource.data){_ in
            self.pickerView?.reloadInputViews()
        }
        /// Add PickerView as Texfield inputView
        textField.inputView = pickerView
    }
    private func setupDatePicker(){
        datePickerView = UIDatePicker.init(frame: .zero)
        if #available(iOS 14.0, *) {
            datePickerView!.preferredDatePickerStyle = .wheels
        }
        datePickerView?.datePickerMode = .date
        datePickerView?.addTarget(self, action: #selector(datePickerChanged(_ :)), for: .valueChanged)
        //km_KH
        textField.inputView = datePickerView
    }
    @objc func datePickerChanged(_ sender: UIDatePicker){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        textField.text = dateFormatter.string(from: sender.date)
        delegate?.textFieldDidChange(textField)
    }
}
extension PickerTextField: UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if isDatePickerStyle == false{
            if let dataSource = self.dataSource.data.value{
                let data = dataSource[selectedIndex]
                if textField.text != data{
                    textField.text = data
                    delegate?.textFieldDidChange(textField)
                }
            }
        }
    }
}
extension PickerTextField: UIPickerViewDelegate{
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return dataSource.data.value?[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    
        selectedIndex = row
    
        if let text = dataSource.data.value?[row]{
            textField.text = text
            delegate?.textFieldDidChange(textField)
        }
    }
}
// MARK: - Data Source Class
class PickerTextFieldDataSource: GenericDataSource<String>, UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return data.value?.count ?? 0
    }
}

