//
//  ProgressHud.swift
//  TestAssessment
//
//  Created by Ouch Kemvanra on 5/25/21.
//

import UIKit

class ProgressHud: UIView {
    
    private static var hud: ProgressHud?
    
    private static let indiCator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView()
        indicator.translatesAutoresizingMaskIntoConstraints = false
        return indicator
    }()
    
    static func showAdded(to: UIView, animated: Bool = true, color: UIColor? = .white, blur: Bool = false, capacity: CGFloat = 0.3, edge: UIEdgeInsets = .zero) -> ProgressHud {
        hud = ProgressHud()
        hud!.translatesAutoresizingMaskIntoConstraints = false
        let constaints = [hud!.topAnchor.constraint(equalTo: to.topAnchor, constant: edge.top),
                          hud!.leadingAnchor.constraint(equalTo: to.leadingAnchor, constant: edge.left),
                          hud!.trailingAnchor.constraint(equalTo: to.trailingAnchor, constant: edge.right),
                          hud!.bottomAnchor.constraint(equalTo: to.bottomAnchor, constant: edge.bottom),
                          
                          indiCator.centerXAnchor.constraint(equalTo: hud!.centerXAnchor),
                          indiCator.centerYAnchor.constraint(equalTo: hud!.centerYAnchor)]
        hud!.backgroundColor = color ?? UIColor.white
        indiCator.color = color == .white ? .blue : .white
        
        if blur {
            hud!.backgroundColor = UIColor.black.withAlphaComponent(capacity)
            indiCator.color = UIColor.white
        }
        to.addSubview(hud!)
        hud!.addSubview(indiCator)
        indiCator.startAnimating()
        NSLayoutConstraint.activate(constaints)
        return hud!
    }
    
    func hide(animated: Bool = true) {
         removeFromSuperview()
    }
    
}

