//
//  MoreView.swift
//  TestAssessment
//
//  Created by Ouch Kemvanra on 5/25/21.
//

import UIKit

class MoreView: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        setTitle("More")
        view.backgroundColor = .init(hexFromString: "#042949")
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationController?.navigationBar.barTransparent(.init(hexFromString: "#042949"))
    }
}
