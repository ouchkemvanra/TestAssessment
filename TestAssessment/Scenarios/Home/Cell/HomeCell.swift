//
//  HomeCell.swift
//  TestAssessment
//
//  Created by Ouch Kemvanra on 5/26/21.
//

import UIKit
class HomeCell: BaseTableViewCell<HomeData> {
    
    private lazy var background: UIView = {
        let v = UIView.init()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.layer.cornerRadius = 20
        v.backgroundColor = .init(hexFromString: "#05335c")
        v.addShadow(offset: .init(width: 4, height: 4), color: .black, radius: 4, opacity: 0.2)
        return v
    }()
    
    private lazy var profileImageView: UIImageView = {
        let imv = UIImageView.init()
        imv.translatesAutoresizingMaskIntoConstraints = false
        imv.layer.cornerRadius = 40
        imv.clipsToBounds = true
        imv.contentMode = .scaleAspectFill
        imv.image = #imageLiteral(resourceName: "userProfile")
        return imv
    }()
    
    private lazy var usernameLabel: UILabel = {
        let txv = UILabel.init()
        txv.translatesAutoresizingMaskIntoConstraints = false
        txv.backgroundColor = .clear
        txv.font = .boldSystemFont(ofSize: 16)
        txv.textColor = .white
        txv.numberOfLines = 1
        return txv
    }()
    
    private lazy var cityLabel: UILabel = {
        let txv = UILabel.init()
        txv.translatesAutoresizingMaskIntoConstraints = false
        txv.backgroundColor = .clear
        txv.font = .systemFont(ofSize: 12)
        txv.numberOfLines = 1
        txv.textColor = .lightText
        return txv
    }()
    private lazy var bookLabel: UILabel = {
        let txv = UILabel.init()
        txv.translatesAutoresizingMaskIntoConstraints = false
        txv.backgroundColor = .clear
        txv.font = .systemFont(ofSize: 14)
        txv.numberOfLines = 1
        txv.textColor = .lightText
        return txv
    }()
    
    private lazy var optionButton: UIButton = {
        let btn = UIButton.init(frame: .zero)
        btn.setImage(#imageLiteral(resourceName: "option"), for: .normal)
        btn.imageView?.contentMode = .scaleAspectFit
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.addTarget(self, action: #selector(didTapOnEdit), for: .touchUpInside)
        return btn
    }()
    
    private var data: HomeData?{
        didSet{
            guard let data = data else{
                usernameLabel.text = nil
                profileImageView.image = nil
                cityLabel.text = nil
                bookLabel.text = nil
                return
            }
            
            let name = String(describing: data.lastName ?? "") + " " + String(describing: data.firstName ?? "")
            let dob = String(describing: data.city ?? "")
            let favoriteBook = String(describing: data.favoriteBook ?? "")
            usernameLabel.text = name
            cityLabel.text = dob
            bookLabel.text = favoriteBook
            
            if let image = data.profileImage{
                profileImageView.image = UIImage(data: image)
            }
        }
    }
    
    override func setView() {
        contentView.backgroundColor = .init(hexFromString: "#042949")
        contentView.addSubview(background)
        contentView.addSubview(profileImageView)
        contentView.addSubview(usernameLabel)
        contentView.addSubview(bookLabel)
        contentView.addSubview(cityLabel)
        contentView.addSubview(optionButton)
        
        NSLayoutConstraint.activate([
            background.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 8),
            background.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -8),
            background.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8),
            background.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -8),
            
            profileImageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 24),
            profileImageView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 24),
            profileImageView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -24),
            profileImageView.widthAnchor.constraint(equalToConstant: 80),
            profileImageView.heightAnchor.constraint(equalToConstant: 80),
            
            usernameLabel.leadingAnchor.constraint(equalTo: profileImageView.trailingAnchor, constant: 16),
            usernameLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -40),
            usernameLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 24),
            usernameLabel.heightAnchor.constraint(lessThanOrEqualToConstant: 60),
            
            cityLabel.topAnchor.constraint(equalTo: usernameLabel.bottomAnchor, constant: 8),
            cityLabel.leadingAnchor.constraint(equalTo: profileImageView.trailingAnchor, constant: 16),
            cityLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -40),
            
            bookLabel.topAnchor.constraint(equalTo: cityLabel.bottomAnchor, constant: 8),
            bookLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -24),
            bookLabel.leadingAnchor.constraint(equalTo: profileImageView.trailingAnchor, constant: 16),
            bookLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -40),
            
            optionButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16),
            optionButton.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 24),
            optionButton.widthAnchor.constraint(equalToConstant: 30),
            optionButton.heightAnchor.constraint(equalToConstant: 15),
        ])
    }
    override func configureData(_ data: HomeData, index: IndexPath) {
        self.data = data
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        data = nil
    }
}
extension HomeCell{
    @objc private func didTapOnEdit(){
        guard let data = data else {return}
        userActionOnCell?(data, "Edit")
    }
}
