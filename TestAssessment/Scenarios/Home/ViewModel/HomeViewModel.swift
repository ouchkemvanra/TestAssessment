//
//  HomeViewModel.swift
//  TestAssessment
//
//  Created by Ouch Kemvanra on 5/25/21.
//

import Foundation

protocol HomeViewModelInput {
    func viewDidLoad()
}

protocol HomeViewModelOutput {
    var state: ((State) -> Void){get set}
}

protocol HomeViewModelType {
    var input: HomeViewModelInput{get}
    var output: HomeViewModelOutput {get set}
}

class HomeViewModel: HomeViewModelType, HomeViewModelInput, HomeViewModelOutput {
    
    var input: HomeViewModelInput{
        return self
    }
    
    var output: HomeViewModelOutput{
        get{
            return self
        } set{
            
        }
    }
    
    var state: ((State) -> Void) = {_ in}
    
    func viewDidLoad() {
        /// Fetcing
        let data = CoreDataManager.shared.getEmployeeHomeData()
        dataSource?.data.value = data
    }
    
    var dataSource: HomeDataSource?
    convenience init(_ dataSource: HomeDataSource) {
        self.init()
        self.dataSource = dataSource
    }
}
