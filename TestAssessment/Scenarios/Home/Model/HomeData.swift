//
//  HomeData.swift
//  TestAssessment
//
//  Created by Ouch Kemvanra on 5/26/21.
//

import UIKit
extension Encodable {
    var dict : [String: Any]? {
        guard let data = try? JSONEncoder().encode(self) else { return nil }
        guard let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String:Any] else { return nil }
        return json
    }
}
struct HomeData: Codable {
    
    var id, firstName, lastName, city, gender, dob, hobby, favoriteBook: String?
    var middleName, address: String?
    var profileImage: Data?
    init() {
        
    }
    init(id: String?, firstName: String?, lastName: String?, middleName: String?, city: String?, gender: String?, dob: String?, hobby: String?, book: String?, address: String?, image: Data?) {
        self.id = id
        self.firstName = firstName
        self.lastName = lastName
        self.city = city
        self.gender = gender
        self.dob = dob
        self.hobby = hobby
        self.favoriteBook = book
        self.middleName = middleName
        self.address = address
        self.profileImage = image
    }
    init(_ emp: Employee) {
        self.id = emp.id
        self.firstName = emp.firstName
        self.middleName = emp.middleName
        self.lastName = emp.lastName
        self.dob = emp.dob
        self.city = emp.city
        self.address = emp.address
        self.hobby = emp.hobby
        self.favoriteBook = emp.favoriteBook
        self.profileImage = emp.profileImage
        self.gender = emp.gender
    }
}
