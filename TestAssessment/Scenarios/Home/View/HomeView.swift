//
//  HomeView.swift
//  TestAssessment
//
//  Created by Ouch Kemvanra on 5/25/21.
//

import UIKit

class HomeView: BaseViewController {
    
    private lazy var tableView: UITableView = {
        let tb = UITableView.init()
        tb.backgroundColor = .init(hexFromString: "#042949")
        tb.showsVerticalScrollIndicator = false
        tb.translatesAutoresizingMaskIntoConstraints = false
        tb.delegate = dataSource
        tb.dataSource = dataSource
        tb.register(HomeCell.self, forCellReuseIdentifier: dataSource.getCellId())
        tb.register(TableEmptyDataCell.self, forCellReuseIdentifier: dataSource.getEmptyCellId())
        tb.contentInset.bottom = view.safeAreaInsets.bottom + 20
        return tb
    }()
    
    private lazy var dataSource: HomeDataSource = HomeDataSource()
    private lazy var viewModel: HomeViewModel = {
        let vm = HomeViewModel.init(dataSource)
        return vm
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.input.viewDidLoad()
    }
    override func binding() {
        viewModel.output.state = {[weak self] state in
            self?.handleState(state)
        }
        dataSource.didSelectOnItem = { [weak self] (data, index) in
            let vc = AddView()
            vc.viewModel = AddViewModel.init(data)
            vc.delegate = self
            let nav = UINavigationController.init(rootViewController: vc)
            self?.present(nav, animated: true, completion: nil)
        }
        dataSource.didActionOnItem = {  [weak self] (data, tag) in
            guard let tagString = tag as? String else {return}
            switch tagString {
                case "Add":
                    self?.addEmployee()
                    break
                case "Edit":
                    let vc = BottomPopupView()
                    vc.computeData = data
                    vc.delegate = self
                    vc.data = [
                        .init(.edit, image: #imageLiteral(resourceName: "edit"), title: "Edit Employee"),
                        .init(.delete, image: #imageLiteral(resourceName: "trash"), title: "Delete Employee")
                    ]
                    self?.present(vc, animated: true, completion: nil)
                    break
                default:
                    break
            }
        }
        tableView.observe(for: dataSource.data){ [weak self] _ in
            self?.tableView.reloadData()
        }
    }
    
    override func setView() {
        setTitle( "Welcome to ABA Employee")
        view.addSubview(tableView)
    }
    override func setConstraint() {
        NSLayoutConstraint.activate([
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.barTransparent()
    }
}
extension HomeView: PopupActionDelegate{
    func action(_ type: PopUpActionType, data: Any) {
        switch type {
            case .delete:
                guard let data = data as? HomeData, let id = data.id else {
                    return
                }
                alert("Are you sure you want to delete this employee?", actions: [
                    .init(title: "Delete", style: .destructive, handler: {[weak self]_ in
                        CoreDataManager.shared.delete(id)
                        self?.viewModel.input.viewDidLoad()
                    }),
                    .init(title: "Cancel", style: .cancel, handler: nil)
                ], completion: nil)
                break
            case .edit:
                guard let data = data as? HomeData else {return}
                let vc = AddView()
                vc.viewModel = AddViewModel.init(data)
                vc.delegate = self
                let nav = UINavigationController.init(rootViewController: vc)
                self.present(nav, animated: true, completion: nil)
                break
            default:
                break
        }
    }
}

extension HomeView{
    public func addEmployee(){
        let vc = AddView()
        vc.viewModel = AddViewModel()
        vc.delegate = self
        let nav = UINavigationController.init(rootViewController: vc)
        self.present(nav, animated: true, completion: nil)
    }
}

extension HomeView : EmployeeDelegate{
    func employeeEdited(_ employee: HomeData?) {
        viewModel.input.viewDidLoad()
    }
    
    func employeeAdded(_ employee: HomeData?) {
        guard let emp = employee else {return}
        dataSource.data.value?.append(emp)
    }
}
