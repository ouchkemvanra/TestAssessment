//
//  BookViewModel.swift
//  TestAssessment
//
//  Created by Ouch Kemvanra on 5/27/21.
//

import Foundation
import SimpleTwoWayBinding

protocol BookViewModelInput {
    func viewDidLoad()
    func selectBook(_ book: BookData)
    func filterResult(_ name: String)
}

protocol BookViewModelOutput {
    var state: ((State) -> Void){get set}
}

protocol BookViewModelType {
    var input: BookViewModelInput{get}
    var output: BookViewModelOutput {get set}
}

class BookViewModel: BookViewModelInput, BookViewModelOutput, BookViewModelType {
    var input: BookViewModelInput{
        return self
    }
    var output: BookViewModelOutput{
        get{
            return self
        } set{
            
        }
    }
    
    var state: ((State) -> Void) = {_ in}
    
    /// Store prop
    let url = "http://api.nytimes.com/svc/books/v3/lists/best-sellers/history.json?api-key=6m8ZoMI3R2AD5LfeiUVMZzQhAQ9GvIOc"
    var searchResult: Observable<[BookData]> = Observable([])
    var dataSource: BookDataSource
    
    init(_ dataSource: BookDataSource) {
        self.dataSource = dataSource
    }
    
    func viewDidLoad() {
        getBook(url)
    }
    func selectBook(_ book: BookData) {
        
    }
    func filterResult(_ name: String) {
        if name != ""{
            let urls = url + "&title=\(name)"
            getBook(urls)
        } else{
            viewDidLoad()
        }
    }
    private func getBook(_ url: String){
        output.state(.fetching)
        Network.shared.request(url){(result : Result<[BookData], Error>) in
            switch result{
                case .success(let data):
                    self.output.state(.success)
                    self.dataSource.data.value = data
                    break
                case .failure(let error):
                    self.output.state(.error(error))
                    break
            }
        }
    }
}
