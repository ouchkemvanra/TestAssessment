//
//  AddViewModel.swift
//  TestAssessment
//
//  Created by Ouch Kemvanra on 5/28/21.
//

import Foundation
import SimpleTwoWayBinding

protocol AddViewModelInput {
    func viewDidLoad()
    func bookSelected(_ book: BookData)
    func addEmployee() -> HomeData?
    func editEmployee() -> HomeData?
}

protocol AddViewModelOutput {
    var state: ((State) -> Void){get set}
    var setData: ((HomeData) -> Void){get set}
}

protocol AddViewModelType {
    var input: AddViewModelInput{get}
    var output: AddViewModelOutput {get set}
}
class AddViewModel: AddViewModelType, AddViewModelInput, AddViewModelOutput {
    var input: AddViewModelInput{
        return self
    }
    var output: AddViewModelOutput{
        get{
            return self
        } set{
            
        }
    }
    
    var state: ((State) -> Void) = {_ in}
    var setData: ((HomeData) -> Void) = {_ in}
    
    /// Store Prop
    var recieveEmployee: HomeData?
    
    var employee: HomeData?
    
    init() {
        self.employee = .init()
    }
    init(_ employee: HomeData) {
        self.recieveEmployee = employee
        self.employee = employee
    }
    
    func viewDidLoad() {
        if let data = self.recieveEmployee{
            output.setData(data)
        }
    }
    func bookSelected(_ book: BookData) {
        
    }
    func addEmployee() -> HomeData? {
        guard var data = self.employee else {return nil}
        if data.id == nil {
            data.id = UUID().uuidString
        }
        CoreDataManager.shared.saveEmployee(data)
        return data
    }
    func editEmployee() -> HomeData? {
        guard let data = self.employee else {return nil}
        CoreDataManager.shared.editEmployee(data)
        return data
    }
}
