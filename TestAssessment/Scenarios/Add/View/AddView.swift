//
//  AddView.swift
//  TestAssessment
//
//  Created by Ouch Kemvanra on 5/27/21.
//

import Foundation
import SkyFloatingLabelTextField
import DLRadioButton
protocol EmployeeDelegate: class {
    func employeeAdded(_ employee: HomeData?)
    func employeeEdited(_ employee: HomeData?)
}
class AddView: BaseViewController {
    let province = [
        "Baat Dambang",
        "Banteay Mean Chey",
        "Kampong Chaam",
        "Kampong Chhnang",
        "Kampong Spueu",
        "Kampong Thum",
        "Kampot",
        "Kandaal",
        "Kaoh Kong",
        "Kracheh",
        "Krong Kaeb",
        "Krong Pailin",
        "Krong Preah Sihanouk",
        "Mondol Kiri",
        "Otdar Mean Chey",
        "Phnom Penh",
        "Pousaat",
        "Preah Vihear",
        "Prey Veaeng",
        "Rotanak Kiri",
        "Siem Reab",
        "Stueng Traeng",
        "Svaay Rieng",
        "Taakaev"
    ]
    private lazy var profileImage : CircularImageView = {
        let imv = CircularImageView.init(image: #imageLiteral(resourceName: "userProfile"))
        imv.contentMode = .scaleAspectFill
        imv.translatesAutoresizingMaskIntoConstraints = false
        return imv
    }()
    private lazy var cameraButton: UIButton = {
        let btn = UIButton.init()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setImage(#imageLiteral(resourceName: "photo-camera-white"), for: .normal)
        btn.layer.cornerRadius = 12.5
        btn.clipsToBounds = true
        btn.backgroundColor = .black
        btn.imageView?.contentMode = .scaleAspectFit
        btn.imageEdgeInsets = .init(top: 6, left: 0, bottom: 6, right: 0)
        btn.addTarget(self, action: #selector(didTapOnImage), for: .touchUpInside)
        return btn
    }()
    private lazy var firstName: SkyFloatingLabelTextField = {
        let txf = SkyFloatingLabelTextField.init(frame: .zero)
        txf.delegate = self
        txf.translatesAutoresizingMaskIntoConstraints = false
        txf.placeholder = "First Name"
        txf.font = .systemFont(ofSize: 10)
        txf.titleFont = .systemFont(ofSize: 10)
        txf.addTarget(self, action: #selector(textFieldDidChanged), for: .editingChanged)
        return txf
    }()
    private lazy var lastName: SkyFloatingLabelTextField = {
        let txf = SkyFloatingLabelTextField.init(frame: .zero)
        txf.translatesAutoresizingMaskIntoConstraints = false
        txf.placeholder = "Last Name"
        txf.font = .systemFont(ofSize: 10)
        txf.titleFont = .systemFont(ofSize: 10)
        txf.addTarget(self, action: #selector(textFieldDidChanged), for: .editingChanged)
        txf.delegate = self
        return txf
    }()
    private lazy var middleName: SkyFloatingLabelTextField = {
        let txf = SkyFloatingLabelTextField.init(frame: .zero)
        txf.translatesAutoresizingMaskIntoConstraints = false
        txf.placeholder = "Middle Name (Optional)"
        txf.font = .systemFont(ofSize: 10)
        txf.titleFont = .systemFont(ofSize: 10)
        txf.addTarget(self, action: #selector(textFieldDidChanged), for: .editingChanged)
        txf.delegate = self
        return txf
    }()
    
    private lazy var address: SkyFloatingLabelTextField = {
        let txf = SkyFloatingLabelTextField.init(frame: .zero)
        txf.translatesAutoresizingMaskIntoConstraints = false
        txf.placeholder = "Address (Optional)"
        txf.font = .systemFont(ofSize: 10)
        txf.titleFont = .systemFont(ofSize: 10)
        txf.addTarget(self, action: #selector(textFieldDidChanged), for: .editingChanged)
        txf.delegate = self
        return txf
    }()
    
    private lazy var hobby: SkyFloatingLabelTextField = {
        let txf = SkyFloatingLabelTextField.init(frame: .zero)
        txf.translatesAutoresizingMaskIntoConstraints = false
        txf.placeholder = "Hobby (Optional)"
        txf.font = .systemFont(ofSize: 10)
        txf.titleFont = .systemFont(ofSize: 10)
        txf.addTarget(self, action: #selector(textFieldDidChanged), for: .editingChanged)
        txf.delegate = self
        return txf
    }()

    private lazy var city: PickerTextField = {
        let txf = PickerTextField.init("City/Province", image: #imageLiteral(resourceName: "down_arrow"))
        txf.isDatePickerStyle = false
        txf.dataSource.data.value =  province
        txf.translatesAutoresizingMaskIntoConstraints = false
        txf.delegate = self
        return txf
    }()
    private lazy var book: SkyFloatingLabelTextField = {
        let txf = SkyFloatingLabelTextField.init()
        txf.translatesAutoresizingMaskIntoConstraints = false
        txf.placeholder = "Favorite Book"
        txf.font = .systemFont(ofSize: 10)
        txf.titleFont = .systemFont(ofSize: 10)
        txf.delegate = self
        return txf
    }()
    private lazy var dob: PickerTextField = {
        let txf = PickerTextField.init("Date of birth (Optional)", image: #imageLiteral(resourceName: "calendar"))
        txf.isDatePickerStyle = true
        txf.translatesAutoresizingMaskIntoConstraints = false
        txf.delegate = self
        return txf
    }()
    private lazy var maleButton: DLRadioButton = {
        let btn = DLRadioButton()
        btn.addTarget(self, action: #selector(genderDidTap), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.isSelected = true
        btn.setTitle("Male", for: .normal)
        btn.setTitleColor(.black, for: .normal)
        btn.titleLabel?.font = .systemFont(ofSize: 12)
        btn.sizeToFit()
        if #available(iOS 11.0, *) {
            btn.contentHorizontalAlignment = .leading
        } else {
            // Fallback on earlier versions
        }
        return btn
    }()
    
    private lazy var femaleButton: DLRadioButton = {
        let btn = DLRadioButton()
        btn.addTarget(self, action: #selector(genderDidTap), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.isSelected = false
        btn.setTitle("Female", for: .normal)
        btn.setTitleColor(.black, for: .normal)
        btn.titleLabel?.font = .systemFont(ofSize: 12)
        btn.sizeToFit()
        if #available(iOS 11.0, *) {
            btn.contentHorizontalAlignment = .leading
        } else {
            // Fallback on earlier versions
        }
        return btn
    }()
    
    private lazy var submitButton: UIButton = {
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setTitle("Save", for: .normal)
        btn.backgroundColor = .init(hexFromString: "#085191")
        btn.setTitleColor(.white, for: .normal)
        btn.titleLabel?.font = .boldSystemFont(ofSize: 14)
        btn.layer.cornerRadius = 8
        btn.layer.masksToBounds = true
        btn.addTarget(self, action: #selector(didTapOnSubmit), for: .touchUpInside)
        return btn
    }()
    
    private lazy var closeBarButton: UIBarButtonItem = {
        let txv = UITextView.init(frame: .zero)
        txv.text = "Cancel"
        txv.textColor = .white
        txv.font = .systemFont(ofSize: 14)
        txv.isSelectable = false
        txv.isEditable = false
        txv.backgroundColor = .clear
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(didTapOnclose))
        txv.addGestureRecognizer(tap)
        let barbutton = UIBarButtonItem.init(customView: txv)
        return barbutton
    }()
    /// view model
    var viewModel: AddViewModel?
    
    /// Store prop
    weak var delegate: EmployeeDelegate?
    private var profile: UIImage?{
        didSet{
            profileImage.image = profile
        }
    }
    private lazy var imagePicker: ImagePicker = {
        let imagePicker = ImagePicker()
        imagePicker.delegate = self
        return imagePicker
    }()
    
    /// Life cycle
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setTitle(viewModel?.employee?.id != nil ? "Employee Information" : "Add New Employee")
        enableSave(false)
        viewModel?.input.viewDidLoad()
        navigationItem.leftBarButtonItems = [closeBarButton]
    }
    override func setView() {
        view.backgroundColor = .white
        view.addSubview(profileImage)
        view.addSubview(firstName)
        view.addSubview(middleName)
        view.addSubview(lastName)
        view.addSubview(maleButton)
        view.addSubview(femaleButton)
        view.addSubview(city)
        view.addSubview(dob)
        view.addSubview(book)
        view.addSubview(address)
        view.addSubview(hobby)
        view.addSubview(submitButton)
        view.addSubview(cameraButton)
        maleButton.otherButtons = [femaleButton]
        femaleButton.otherButtons = [maleButton]
        let gesture = UITapGestureRecognizer.init(target: self, action: #selector(didTapOnImage))
        profileImage.addGestureRecognizer(gesture)
        profileImage.isUserInteractionEnabled = true
        profileImage.makeCircular()
    }
    override func setConstraint() {
        NSLayoutConstraint.activate([
            
            profileImage.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 8),
            profileImage.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            profileImage.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.2),
            profileImage.widthAnchor.constraint(equalTo: profileImage.heightAnchor),
            
            cameraButton.bottomAnchor.constraint(equalTo: profileImage.bottomAnchor),
            cameraButton.trailingAnchor.constraint(equalTo: profileImage.trailingAnchor, constant: -24),
            cameraButton.widthAnchor.constraint(equalToConstant: 25),
            cameraButton.heightAnchor.constraint(equalToConstant: 25),
            
            firstName.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16),
            firstName.trailingAnchor.constraint(equalTo: view.centerXAnchor, constant: 24),
            firstName.topAnchor.constraint(equalTo: profileImage.bottomAnchor, constant: 8),
            firstName.heightAnchor.constraint(equalToConstant: 40),
            
            middleName.leadingAnchor.constraint(equalTo: firstName.trailingAnchor, constant: 8),
            middleName.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16),
            middleName.topAnchor.constraint(equalTo: profileImage.bottomAnchor, constant: 8),
            middleName.heightAnchor.constraint(equalToConstant: 40),
            
            lastName.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16),
            lastName.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16),
            lastName.topAnchor.constraint(equalTo: firstName.bottomAnchor, constant: 8),
            lastName.heightAnchor.constraint(equalToConstant: 40),
            
            maleButton.leadingAnchor.constraint(equalTo: firstName.leadingAnchor),
            maleButton.widthAnchor.constraint(equalToConstant: 100),
            maleButton.topAnchor.constraint(equalTo: lastName.bottomAnchor, constant: 16),
            
            femaleButton.leadingAnchor.constraint(equalTo: maleButton.trailingAnchor, constant: 16),
            femaleButton.widthAnchor.constraint(equalToConstant: 100),
            femaleButton.topAnchor.constraint(equalTo: lastName.bottomAnchor, constant: 16),
            
            address.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16),
            address.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16),
            address.topAnchor.constraint(equalTo: femaleButton.bottomAnchor, constant: 8),
            address.heightAnchor.constraint(equalToConstant: 40),
            
            city.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16),
            city.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16),
            city.topAnchor.constraint(equalTo: address.bottomAnchor, constant: 8),
            city.heightAnchor.constraint(equalToConstant: 40),
            
            dob.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16),
            dob.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16),
            dob.topAnchor.constraint(equalTo: city.bottomAnchor, constant: 8),
            dob.heightAnchor.constraint(equalToConstant: 40),
            
            book.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16),
            book.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16),
            book.topAnchor.constraint(equalTo: dob.bottomAnchor, constant: 8),
            book.heightAnchor.constraint(equalToConstant: 40),
            
            hobby.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16),
            hobby.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16),
            hobby.topAnchor.constraint(equalTo: book.bottomAnchor, constant: 8),
            hobby.heightAnchor.constraint(equalToConstant: 40),
            
            submitButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -8),
            submitButton.heightAnchor.constraint(equalToConstant: 40),
            submitButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16),
            submitButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16),
        ])
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.barTransparent(.init(hexFromString: "#042949"))
    }
    override func binding() {
        viewModel?.output.setData = {[weak self] data in
            self?.firstName.text = data.firstName
            self?.lastName.text = data.lastName
            self?.book.text = data.favoriteBook
            self?.address.text = data.address
            self?.city.textField.text = data.city
            self?.city.selectedIndex = self?.province.firstIndex(where: {data.city == $0}) ?? 0
            self?.dob.textField.text = data.dob?.toDateString()
            self?.middleName.text = data.middleName
            self?.hobby.text = data.hobby
            if let image = data.profileImage{
                self?.profile = UIImage(data: image)
            }
            if data.gender == "F"{
                self?.femaleButton.isSelected = true
            } else{
                self?.maleButton.isSelected = true
            }
        }
    }
}
extension AddView{
    
    @objc private func genderDidTap(){
        enableSave(validateInfo())
    }
    
    @objc private func textFieldDidChanged(_ textField: SkyFloatingLabelTextField){
        let text = textField.text
        switch textField {
            case firstName:
                self.viewModel?.employee?.firstName = text
            case lastName:
                self.viewModel?.employee?.lastName = text
            case middleName:
                self.viewModel?.employee?.middleName = text
            case address:
                self.viewModel?.employee?.address = text
            case hobby:
                self.viewModel?.employee?.hobby = text
            default:
                break
        }
        enableSave(validateInfo())
    }
    
    @objc private func didTapOnImage(){
        let vc = BottomPopupView()
        vc.data = [
            .init(.camera, image: #imageLiteral(resourceName: "upload_via_camera"), title: "Upload via camera"),
            .init(.photoGallery, image: #imageLiteral(resourceName: "upload_via_gallery"), title: "Upload via photo gallery")
        ]
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc private func didTapOnclose(){
        self.dismiss(animated: true, completion: nil)
    }
    @objc private func didTapOnSubmit(){
        print("Male \(maleButton.isSelected)")
        viewModel?.employee?.gender = maleButton.isSelected ? "M":"F"
        let data = self.viewModel?.recieveEmployee == nil ? viewModel?.input.addEmployee() : viewModel?.input.editEmployee()
        self.dismiss(animated: true){
            if self.viewModel?.recieveEmployee == nil{
                self.delegate?.employeeAdded(data)
            } else{
                self.delegate?.employeeEdited(data)
            }
        }
    }
    
    private func validateInfo() -> Bool{
        guard let lastname = lastName.text else { return false}
        guard let firstname = firstName.text else { return  false}
        guard let city = city.textField.text else { return  false}
        guard let book = book.text else { return false}
        let gender = maleButton.isSelected ? "M":"F"
        if lastname == "" {
            return false
        }
        if firstname == "" {
            return false
        }
        if city == "" {
            return false
        }
        if book == "" {
            return false
        }
        if profile == nil{
            return false
        }
        return true
    }
    
    private func enableSave(_ enable: Bool){
        submitButton.backgroundColor = enable ? .init(hexFromString: "#085191"):.lightGray
        submitButton.isUserInteractionEnabled = enable
    }
    
    private func checkErrorTextField(_ textField: SkyFloatingLabelTextField, isError: Bool, message: String =  " "){
        if isError{
            textField.errorMessage = message
        } else{
            textField.errorMessage = nil
        }
    }
}
extension AddView: PopupActionDelegate{
    func action(_ type: PopUpActionType, data: Any) {
        switch type {
            case .camera:
                imagePicker.cameraAsscessRequest()
                break
            case .photoGallery:
                imagePicker.photoGalleryAsscessRequest()
                break
            default:
                break
        }
    }
}
extension AddView: PickerTextFieldDelegate{
    func textFieldDidChange(_ textField: UITextField) {
        if textField == city.textField{
            self.viewModel?.employee?.city = textField.text
        } else if textField == dob.textField{
            self.viewModel?.employee?.dob = textField.text
        }
        enableSave(validateInfo())
    }
}
extension AddView: UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == book{
            let vc = BookSelectionView()
            vc.delegate = self
            self.navigationController?.pushViewController(vc, animated: true)
            textField.resignFirstResponder()
            return false
        }
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let txf = textField as? SkyFloatingLabelTextField{
            if txf.placeholder?.contains("Optional") == false{
                checkErrorTextField(txf, isError: txf.text == "" || txf.text == nil)
            }
        }
    }
}
extension AddView: BookSelectionDelegate{
    func selectBook(_ book: BookData) {
        self.book.text = book.title
        self.viewModel?.employee?.favoriteBook = book.title
        self.enableSave(validateInfo())
    }
}

// MARK: - Image Picker Delegate
extension AddView: ImagePickerDelegate{
    func imagePicker(_ imagePicker: ImagePicker, didSelect image: UIImage) {
        viewModel?.employee?.profileImage = image.pngData()
        profile = image
        imagePicker.dismiss()
        enableSave(validateInfo())
    }

    func cancelButtonDidClick(on imageView: ImagePicker) {
        imagePicker.dismiss()
    }
    func imagePicker(_ imagePicker: ImagePicker, grantedAccess: Bool,
                   to sourceType: UIImagePickerController.SourceType) {
        guard grantedAccess else { return }
        imagePicker.present(parent: self, sourceType: sourceType)
    }
}
extension UIImage{
    func resizedImage(Size sizeImage: CGSize) -> UIImage?{
        let frame = CGRect(origin: CGPoint.zero, size: CGSize(width: sizeImage.width, height: sizeImage.height))
        UIGraphicsBeginImageContextWithOptions(frame.size, false, 0)
      self.draw(in: frame)
        let resizedImage: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.withRenderingMode(.alwaysOriginal)
        return resizedImage
    }
}

