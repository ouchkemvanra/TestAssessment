//
//  BookSelectionView.swift
//  TestAssessment
//
//  Created by Ouch Kemvanra on 5/27/21.
//

import UIKit
protocol BookSelectionDelegate: class {
  func selectBook(_ book: BookData)
}

class BookSelectionView: BaseViewController {
    private lazy var tableView: UITableView = {
        let tb = UITableView.init()
        tb.estimatedRowHeight = 60
        tb.translatesAutoresizingMaskIntoConstraints = false
        tb.delegate = dataSource
        tb.dataSource = dataSource
        tb.register(BookCell.self, forCellReuseIdentifier: dataSource.getCellId())
        tb.keyboardDismissMode = .interactive
        return tb
    }()
    /// Store Properties
    weak var delegate: BookSelectionDelegate?
    private lazy var dataSource: BookDataSource = {
        return BookDataSource()
    }()
    private lazy var viewModel: BookViewModel = {
        return .init(dataSource)
    }()
    
    /// Search Controller
    let searchController = UISearchController(searchResultsController: nil)
    
    /// Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTable()
        viewModel.input.viewDidLoad()
    }
    override func setView() {
        setTitle("Select Your Favorite Book")
        view.addSubview(tableView)
    }
    override func setConstraint() {
        NSLayoutConstraint.activate([
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.barTransparent(.init(hexFromString: "#042949"))
    }
    override func binding() {
        self.tableView.observe(for: viewModel.dataSource.data){[weak self] _ in
            self?.tableView.reloadData()
        }
        dataSource.didSelectOnItem = {[weak self] (data, index) in
            self?.delegate?.selectBook(data)
            self?.view.endEditing(true)
            self?.navigationController?.popViewController(animated: true)
        }
        viewModel.output.state = {[weak self] state in
            self?.handleState(state)
        }
    }
}
//MARK: - Setup View Layout
extension BookSelectionView{
    
    /// Setup search controller
    private func setupSearchController(){
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchResultsUpdater = self
        self.definesPresentationContext = true
        tableView.tableHeaderView = searchController.searchBar
    }
    
    /// Setup Table
    private func setupTable(){
        setupSearchController()
    }
}
// MARK: - UISearch Result Updating
extension BookSelectionView: UISearchResultsUpdating{
    func updateSearchResults(for searchController: UISearchController) {
        if let searchText = searchController.searchBar.text {
            viewModel.input.filterResult(searchText)
            // Reload the table view with the search result data.
            tableView.reloadData()
        }
    }
}

