//
//  BookCell.swift
//  TestAssessment
//
//  Created by Ouch Kemvanra on 5/27/21.
//

import UIKit
class BookCell: BaseTableViewCell<BookData>{
    override func configureData(_ data: BookData, index: IndexPath) {
        self.textLabel?.text = data.title
    }
}
