//
//  Book.swift
//  TestAssessment
//
//  Created by Ouch Kemvanra on 5/27/21.
//

import Foundation
class BookData: Codable {
    var title: String?
    
    enum CodingKeys: String, CodingKey {
        case title
    }
}
