//
//  Network.swift
//  TestAssessment
//
//  Created by Ouch Kemvanra on 5/28/21.
//

import Foundation
import SwiftyJSON
class NetworkResult<T: Codable>: Codable{
    var status: String?
    var results: [T]?
}
class Network {
    static let shared = Network()
    func request<T: Codable>(_ urlString: String, completion: @escaping(Result<[T], Error>)->()){
        guard let url = URL(string: urlString) else {return}
        print(url)
        URLSession.shared.dataTask(with: url) { (data, res, err) in
            if err != nil {
                DispatchQueue.main.async {
                    completion(.failure(err!))
                }
            }
            guard let data = data else {return}
            do{
                print(JSON(data))
                let retrievedData = try JSONDecoder().decode(NetworkResult<T>.self, from: data)
                DispatchQueue.main.async {
                    completion(.success(retrievedData.results ?? []))
                }
            }catch let jsonErr {
                DispatchQueue.main.async {
                    completion(.failure(jsonErr))
                }
            }
        }.resume()
    }
}
