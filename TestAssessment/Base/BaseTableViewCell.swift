//
//  BaseTableViewCell.swift
//  TestAssessment
//
//  Created by Ouch Kemvanra on 5/25/21.
//

import UIKit
class BaseTableViewCell<D: Decodable>: UITableViewCell {
    var userActionOnCell: ((_ item: D?, _ tag: String) -> ())?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureData(_ data: D, index: IndexPath){
        
    }
    func setView(){
        
    }
}
