//
//  CoreDataObject.swift
//  TestAssessment
//
//  Created by Ouch Kemvanra on 5/27/21.
//

import Foundation
import CoreData
class DecoderWrapper: Decodable {

    let decoder:Decoder

    required init(from decoder:Decoder) throws {

        self.decoder = decoder
    }
}

protocol JSONDecoding {
     func decodeWith(_ decoder: Decoder) throws
}

extension JSONDecoding where Self:NSManagedObject {

    func decode(json:[String:Any]) throws {

        let data = try JSONSerialization.data(withJSONObject: json, options: [])
        let wrapper = try JSONDecoder().decode(DecoderWrapper.self, from: data)
        try decodeWith(wrapper.decoder)
    }
}

extension Employee: JSONDecoding {

    enum CodingKeys: String, CodingKey {
        case firstName, lastName, middleName, city, address, hobby, dob, favoriteBook, gender, profileImage, id
    }

    func decodeWith(_ decoder: Decoder) throws {

        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.firstName = try container.decodeIfPresent(String.self, forKey: .firstName)
        self.lastName = try container.decodeIfPresent(String.self, forKey: .lastName)
        self.middleName = try container.decodeIfPresent(String.self, forKey: .middleName)
        self.id = try container.decodeIfPresent(String.self, forKey: .id)
        self.city = try container.decodeIfPresent(String.self, forKey: .city)
        self.address = try container.decodeIfPresent(String.self, forKey: .address)
        self.gender = try container.decodeIfPresent(String.self, forKey: .gender)
        self.hobby = try container.decodeIfPresent(String.self, forKey: .hobby)
        self.dob = try container.decodeIfPresent(String.self, forKey: .dob)
        self.favoriteBook = try container.decodeIfPresent(String.self, forKey: .favoriteBook)
        self.profileImage = try container.decodeIfPresent(Data.self, forKey: .profileImage)
    }
    func updateFromHomeData(_ data: HomeData){
        self.firstName = data.firstName
        self.lastName = data.lastName
        self.middleName = data.middleName
        self.city = data.city
        self.address = data.address
        self.gender = data.gender
        self.hobby = data.hobby
        self.dob = data.dob
        self.favoriteBook = data.favoriteBook
        self.profileImage = data.profileImage
    }
}
