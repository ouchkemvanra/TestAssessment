//
//  CoreDataManager.swift
//  TestAssessment
//
//  Created by Ouch Kemvanra on 5/26/21.
//

import UIKit
import CoreData
import SwiftyJSON
class CoreDataManager {
    static var shared = CoreDataManager()
    var context = (UIApplication.shared.delegate
                          as! AppDelegate).persistentContainer.viewContext
}
extension CoreDataManager{
    /**
    getEmployees
    -  returns: [Employee]
    */
    func getEmployees() -> [Employee]?{
      do{
          let result = try context.fetch(Employee.fetchRequest()) as? [Employee]
          return result
      } catch{
          return nil
      }
    }
    
    func getEmployeeHomeData() -> [HomeData]?{
        if let data = getEmployees(){
            let homeList = data.map({ emp in return HomeData.init(emp)})
            return homeList
        } else{
            return nil
        }
    }
    
    func saveEmployee(_ employee: HomeData){
        guard let dict = employee.dict else {return}
        let entity = NSEntityDescription.insertNewObject(forEntityName: "Employee", into: context) as! Employee
        do {
            try entity.decode(json: dict)
            save()
        } catch{
            
        }
    }
    func editEmployee(_ employee: HomeData){
        guard let id = employee.id else {return}
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Employee")
        fetchRequest.predicate = NSPredicate(format: "(id = %@)", id)
        do{
            let fetchResult = try context.fetch(fetchRequest) as? [Employee]
            if fetchResult?.isEmpty == false{
                let obj = fetchResult?.first
                obj?.updateFromHomeData(employee)
                save()
            }
        } catch{
            
        }
    }
    
    func delete(_ id: String){
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Employee")
        fetchRequest.predicate = NSPredicate(format: "(id = %@)", id)
        do{
            let fetchResult = try context.fetch(fetchRequest) as? [NSManagedObject]
            if fetchResult?.isEmpty == false{
                guard let obj = fetchResult?.first else {return}
                context.delete(obj)
                save()
            }
        } catch{
            
        }
    }
  
    func save(){
        do{
           try context.save()
        } catch{
            print("Error Save")
        }
    }
    func testSave(){
        let image : UIImage = #imageLiteral(resourceName: "userProfile")
        let data = image.base64String()
        
        let json = [
            "firtName": "User",
            "lastName": "Test",
            "middleName": "One",
            "city": "PP",
            "dob": "2016-04-14T10:44:00+0000",
            "hobby": "Football",
            "gender": "Male",
            "profileImage": data,
            "favoriteBook": "Harry Potter",
            "id": "1213"
        ] as [String : Any]
        let entity = NSEntityDescription.insertNewObject(forEntityName: "Employee", into: context) as! Employee
        do {
            try entity.decode(json: json)
            save()
        } catch{
            
        }
    }
}
extension UIImage{
    
    func base64String() -> String {
        return self.jpegData(compressionQuality: 1)?.base64EncodedString() ?? ""
    }
}
extension String{
    func base64StringToImage () -> UIImage {
        let imageData = Data.init(base64Encoded: self, options: .init(rawValue: 0))
        let image = UIImage(data: imageData!)
        return image!
    }
}
