//
//  BaseTableView.swift
//  TestAssessment
//
//  Created by Ouch Kemvanra on 5/25/21.
//

import UIKit
extension UIViewController{
    func setTitle(_ title: String) {
        let labelNavTitle = UITextView()
        labelNavTitle.backgroundColor = .none
        labelNavTitle.text = title
        labelNavTitle.sizeToFit()
        labelNavTitle.font = .boldSystemFont(ofSize: 16)
        labelNavTitle.textColor = .white
        labelNavTitle.textContainer.maximumNumberOfLines = 1
        labelNavTitle.textContainer.lineBreakMode = .byTruncatingTail
        labelNavTitle.isEditable = false
        labelNavTitle.isUserInteractionEnabled = false
        self.navigationItem.titleView = labelNavTitle
    }
}
class BaseViewController: UIViewController {
    
    /// loading Hud
    var hud: ProgressHud?
    
    /// Init
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /// Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setView()
        setConstraint()
        binding()
    }
    
    /// Handle State binding
    func handleState(_ state: State){
        switch state {
            case .fetching:
                showHud()
                break
            default:
                hideHud()
                break
        }
    }
    
    /// Handle Error
    func handleError(_ error: Error){
        
    }
    
    /// set view override needed
    public func setView(){
        
    }
    
    ///set constraint override needed
    public func setConstraint(){
        
    }
    
    /// viewmodel binding
    func binding(){
        
    }
    
    /// Hide Hud
    func hideHud() {
        hud?.hide()
    }
    
    /// Show Hud
    func showHud() {
        hud?.hide()
        hud = .showAdded(to: view)
    }
}
