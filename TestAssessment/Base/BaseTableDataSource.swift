//
//  BaseTableDataSource.swift
//  TestAssessment
//
//  Created by Ouch Kemvanra on 5/25/21.
//

import UIKit
class BaseTableDataSource<D: Codable, CELL: BaseTableViewCell<D>>: GenericDataSource<D>, UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let data = data.value else {
            return 0
        }
        if data.isEmpty{
            /// Show Empty Data View
            return 1
        }
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let data = data.value else{
            /// Null
            return UITableViewCell()
        }
        if data.isEmpty{
            /// Show Empty View
            let cell = tableView.dequeueReusableCell(withIdentifier: getEmptyCellId()) as! TableEmptyDataCell
            cell.configureData(.init(), index: indexPath)
            cell.userActionOnCell = {[weak self] (data, tag) in
                self?.didActionOnItem?(data as? D, tag)
            }
            return cell
        } else{
            /// Has data
            let cell = tableView.dequeueReusableCell(withIdentifier: getCellId()) as! CELL
            let item = data[indexPath.row]
            cell.configureData(item, index: indexPath)
            cell.userActionOnCell = {[weak self] (data, tag) in
                self?.didActionOnItem?(data, tag)
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard  let data = data.value else {
            return 0
        }
        if data.isEmpty{
            return tableView.bounds.height
        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let data  = data.value , !data.isEmpty else {
            return
        }
        let item = data[indexPath.row]
        didSelectOnItem?(item, indexPath)
    }
    
    func getCellId() -> String{
        return "cellID"
    }
    
    func getEmptyCellId() -> String{
        return "emptyCellID"
    }
}
